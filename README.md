## IflyLabs Task

This is the task from IflyLabs and is made using Angular 2 framework. It uses typescript for coding and webpack for bundling and dev-server. It also uses the reactive library of Rxjs.
The project is buid using the seed pack by Angular at - https://github.com/angular/angular2-seed


### Usage
- Clone or fork this repository
- Make sure you have [node.js](https://nodejs.org/) installed version 5+
- Make sure you have NPM installed version 3+
- `WINDOWS ONLY` run `npm install -g webpack webpack-dev-server typescript` to install global dependencies
- run `npm install` to install dependencies
- run `npm start` to fire up dev server
- open browser to [`http://localhost:3000`](http://localhost:3000)
- if you want to use other port, open `package.json` file, then change port in `--port 3000` script
