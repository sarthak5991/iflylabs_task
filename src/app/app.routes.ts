import { Routes } from '@angular/router';

import { HomePageComponent } from './home.page.component';

export const rootRouterConfig: Routes = [
  { path: '', pathMatch: 'full', component: HomePageComponent },
];

