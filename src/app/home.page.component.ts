import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';

import { Service } from './service';

@Component({
	selector: 'home-page',
	templateUrl: './home.page.component.html',
	styleUrls: ['./home.page.component.css']
})
export class HomePageComponent implements OnInit {
	totalUsersRetrieved: number;
	totalPages: number;
	currentPageNumber: number = 1;
	currentPageList: Array<any>;
	currentList: Array<any>;
	private canMakeRequest: boolean = false;
	stock: any;

	constructor(private service: Service) { }

	ngOnInit() {
		this.service.getData()
			.subscribe((value:any) => {
				this.currentList = value;
				this.setVariableValues(this.currentList);
			}, (Error) => {
				console.log(Error);
				return;
			}, () => {
		});

		this.continueProcess();
	}

	onChange(event) {
	    let file = event.srcElement.files;
	    let postData = {test: 'test'}; // Put your form data variable. This is only example.
	    this.service.postWithFile("HERE GOES URL",postData,file).then(result => {
	        console.log(result);
	    });
	}

	private setVariableValues(rawData: Array<any>) {
		if(rawData != null&&rawData!= undefined) {
			this.totalUsersRetrieved = rawData[rawData.length-1].o;
			this.totalPages = Math.ceil(this.totalUsersRetrieved/10);

			this.setCurrentPageList(this.currentPageNumber);
		}
	}

	private setCurrentPageList(pageNo: number) {
		let first = (pageNo-1)*10;
		let last = pageNo==this.totalPages ? this.currentList.length - 2 : first+10;
		this.currentPageList = this.currentList.slice(first, last);
	}

	private continueProcess() {	
		Observable.interval(1000)
			.switchMap(() => {
				if(this.canMakeRequest)
					return this.service.getData();
				else {
					throw new Error('error');
				}
			})
			.catch((error) => error)
			.subscribe((value: any) => {
				this.currentList = value
				this.setVariableValues(this.currentList);
			}, (Error) => {
				console.log(Error);
				return;
			}, () => {
		});
	}

	previousClicked() {
		this.currentPageNumber = this.currentPageNumber>1 ? this.currentPageNumber - 1 : this.currentPageNumber;
		this.setCurrentPageList(this.currentPageNumber);
	}

	nextClicked() {
		this.currentPageNumber = this.currentPageNumber<this.totalPages ? this.currentPageNumber + 1 : this.currentPageNumber;
		this.setCurrentPageList(this.currentPageNumber);
	}

	startClicked() {
		this.canMakeRequest = true;
		this.continueProcess();
	}

	stopClicked() {
		this.canMakeRequest = false;
	}
} 