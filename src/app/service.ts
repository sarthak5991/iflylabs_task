import { Injectable } from '@angular/core';
import { Http, Response, Headers} from '@angular/http';

import { Observable } from 'rxjs';

@Injectable() 
export class Service {
	private url: string = 'https://api.iflychat.com/users/list/demo/c-g';
	private file:File;
	responseData: any;
	constructor(private http: Http) { }

	getData(): Observable<Response> {
		return this.http.get(this.url)
				.map(this.extractData)
				.catch(this.handleError);
	}

	postWithFile(url, postData:any, files: File[]) {
		let headers = new Headers();	
		let formData: FormData = new FormData();
		formData.append('files', files[0], files[0].name);

		if(postData !=="" && postData !== undefined && postData !==null){
			for (var property in postData) {
			  	if (postData.hasOwnProperty(property)) {
			      formData.append(property, postData[property]);
			  	}
			}
    	}
	    var returnReponse = new Promise((resolve, reject) => {
	      this.http.post(url, formData, {
	        headers: headers
	      }).subscribe(
	          res => {
	            this.responseData = res.json();
	            console.log(this.responseData);
	            resolve(this.responseData);
	          },
	          error => {
	            // this.router.navigate(['/login']);
	            reject(error);
	          }
	      );
	    });
	    return returnReponse;
	}

	private extractData(res: Response) {
		let body = res.json();
		return body || { };
	}

	private handleError(error: any) {
		let errMsg = (error.message) ? error.message : error.status ? `${error.status} - ${error.statusText}` : 'Server error';
		console.error(errMsg);
		return Observable.throw(error);
	}
}